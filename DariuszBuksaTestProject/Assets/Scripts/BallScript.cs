﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BallScript : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private Transform ballHook;
    [SerializeField] private float lineLength = 1.5f;
    [SerializeField] private float attractionStrength = 2f;

    LineRenderer lineRend;
    Rigidbody2D rb2D;

    private 
    // Start is called before the first frame update
    void Start()
    {
        lineRend = gameObject.GetComponent<LineRenderer>();
        lineRend.positionCount = 2;
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3[] positions = { ballHook.position, transform.position };
        lineRend.SetPositions(positions);

        float dist = Vector3.Distance(ballHook.position, transform.position);
        if (dist > lineLength)
        {
            Vector3 dir = ballHook.position - transform.position;
            rb2D.transform.position += attractionStrength * dir.normalized * Time.deltaTime;
            
        }
        //rb2D.velocity * ;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Start");
    }
}
