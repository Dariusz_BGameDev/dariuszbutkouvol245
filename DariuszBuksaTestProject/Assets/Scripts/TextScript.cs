﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScript : MonoBehaviour
{
    [SerializeField] Text text;

    public int _counter { get; private set; }
    public static TextScript Instance;
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void IncrementCounter()
    {
        _counter++;
        UpdateText();
    }

    public void DecrementCounter()
    {
        _counter++;
        UpdateText();
    }

    void UpdateText()
    {
        text.text = "Tower: " + _counter;
    }
}
