﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField] GameObject towerPrefab;

    Rigidbody rb;
    float randomDistance;

    Vector3 startPos;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        randomDistance = Random.Range(1, 4);
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        rb.transform.position += transform.TransformDirection(Vector3.up) * 4 * Time.deltaTime;
        

        if (Vector3.Distance(startPos, transform.position) > randomDistance)
        {
            SpawnTower();
            Destroy(gameObject);
        }
    }

    void SpawnTower()
    {
        Instantiate(towerPrefab, transform.position, towerPrefab.transform.rotation);
    }

}
