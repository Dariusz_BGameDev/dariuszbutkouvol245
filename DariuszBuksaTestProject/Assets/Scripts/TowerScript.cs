﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScript : MonoBehaviour
{
    [SerializeField] float delay = 6f;
    [SerializeField] Transform cannonTransform;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] Transform bulletPool;
    // Start is called before the first frame update

    int _counter = 12;
    SpriteRenderer spriteRend;
    void Start()
    {
        TextScript.Instance.IncrementCounter();
        spriteRend = GetComponent<SpriteRenderer>();
        StartCoroutine(Rotate());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Rotate()
    {
        spriteRend.material.color = Color.white;
        yield return new WaitForSecondsRealtime(delay);
        spriteRend.material.color = Color.red;
        while (_counter > 0)
        {
            transform.Rotate(Vector3.forward, Random.Range(15f, 45f));
            Shoot();
            yield return new WaitForSecondsRealtime(0.5f);
        }
        spriteRend.material.color = Color.white;
        yield return null;
    }

    void Shoot()
    {
        if (_counter > 0)
        {
            _counter--;
            Instantiate(bulletPrefab, cannonTransform.position + new Vector3(0f, 0.2f, 0f), transform.rotation, bulletPool);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        TextScript.Instance.DecrementCounter();
        Destroy(collision.gameObject);
        Destroy(gameObject);
    }
}
